public class JavaStrings3 {
    public static void main(String[] args) {
        String txt = "Hello World";
        System.out.println(txt.toUpperCase()); // outputs "HELLO WORLD"
        System.out.println(txt.toLowerCase());  // Outputs "hello world"

    }
}
