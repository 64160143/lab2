public class JavaTypeCasting1 {
    public static void main(String[] args) {
        int myint = 9;
        double mydouble = myint; // Automatic casting; int to double

        System.out.println(myint); //Outputs 9
        System.out.println(mydouble);  //Output
    }
}

