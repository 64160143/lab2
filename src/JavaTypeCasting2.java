public class JavaTypeCasting2 {
    public static void main(String[] args) {
        double mydouble = 9.78d;
        int myint = (int) mydouble; //Manual casting: double to int

        System.out.println(mydouble);  //output 9.78
        System.out.println(myint);     //output9
    }
}
